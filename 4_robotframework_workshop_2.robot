*** Settings ***
Library  BuiltIn
Library  SeleniumLibrary
Library  OperatingSystem

*** Variables ***
${url}=  http://ptt-devpool-robotframework.herokuapp.com/
${input}=  Robotframework
${username}=  admin
${password}=  1234
&{1}=  firstname=David  lastname=Beckham  email=David.Beckham@gmail.com
&{2}=  firstname=Johnny  lastname=Deep  email=Johnny.Deep@gmail.com
&{3}=  firstname=Robert  lastname=Downey  email=Robert.Downey@gmail.com
&{4}=  firstname=Tom  lastname=Hanks  email=Tom.Hanks@gmail.com

*** Test Cases ***
1. เปิด Browser
    Create Webdriver  Chrome  executable_path=C:/Program Files (x86)/Google/Chrome/Application/chromedriver.exe
    Open Browser  about:blank  browser=Chrome  
    Maximize Browser Window
    Set Selenium Speed  0.5

2. ไปที่เว็บไซต์ 
    Go To  ${url}

3. กรอก login
    Input Text  id=txt_username  ${username}
    Input Text  id=txt_password  ${password}

4.click submit login
    click Button  id=btn_login

5.กรอกข้อมูล
    FOR  ${i}  IN RANGE  1  5
    Click Button  id=btn_new
    Input Text  id=txt_new_firstname  ${${i}}[firstname]
    Input Text  id=txt_new_lastname  ${${i}}[lastname]
    Input Text  id=txt_new_email  ${${i}}[email]
    Click Button  id=btn_new_save
    END

6.Capture Page
    Capture Page Screenshot  filename=${CURDIR}/screenshot.png

